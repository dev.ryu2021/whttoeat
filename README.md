# Wht to eat 🍙

내가 먹고 싶은 점심 menu 4가지를 화면에 추가하고, 그 중에 1가지를 랜덤하게 선택해주는 웹앱입니다만... 현재는 미완성인 상태입니다! 아래의 3가지 스텝을 통해서 정상적으로 작동할 수 있게 만들어주세요~

<br />

![](/sample.png)

<br />


## 👨‍🎓 Student
---
<br />

🙈 아래의 3가지 스텝으로 앱을 완성시켜 주세요
<br />

1: 첫 화면에 진입시 내가 먹고 싶은 메뉴 4가지가 보이게 해주세요.<br />
2: 4가지 메뉴 중에 1가지를 선택해주는 버튼을 만들어주세요.<br />
3: 버튼을 눌렀을 때, 랜덤하게 선택된 메뉴가 화면에 보이게 해주세요.<br />

<br />

🙊 Checklist
<br />

[ ] 진입 시, 메뉴 4가지가 보이는가? <br />
[ ] 메뉴를 선택할 수 있는 버튼이 있는가? <br />
[ ] 버튼을 누르면 메뉴선택이 되는가?

<br />
<br />

## 👨‍💻 Instructor 
---

<br />

### 1. Setup
1: install vscode <br />
2: install golive extension <br />

<br />

### 2. Contents
1: let students know what kinds of types JS has. (string, number, boolean) <br />
2: let students know how to declare and assign variables.<br />
3: let students know how to declare function and how to call it.<br />
4: let students know what is conditional statement and how to use it.<br />
